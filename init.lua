--------------------------------------------------------
-- Minetest :: Navigator Mod v1.0 (navigator)
--
-- See README.txt for licensing and release notes.
-- Copyright (c) 2016-2020, Leslie E. Krause
--
-- ./games/minetest_game/mods/navigator/init.lua
--------------------------------------------------------

local player_huds = { }
local globaltimer = Timekeeper { }

-----------------

local dir_names = {
	["N"] = "north",
	["NE"] = "northeast",
	["E"] = "east",
	["SE"] = "southeast",
	["S"] = "south",
	["SW"] = "southwest",
	["W"] = "west",
	["NW"] = "northwest",
	["U"] = "up",
	["D"] = "down",
}

local function coords_to_string( pos )
        local pos_x = math.floor( pos.x + 0.5 )
        local pos_z = math.floor( pos.z + 0.5 )

        local s1 = pos_x < 0 and ( -pos_x .. " W" ) or ( pos_x .. " E" )
        local s2 = pos_z < 0 and ( -pos_z .. " S" ) or ( pos_z .. " N" )

        return s1 .. ", " .. s2
end

local function to_facing( dir )
	local dx = math.floor( dir.x + 0.5 )
	local dz = math.floor( dir.z + 0.5 )

	if dx == 0 and dz == 0 then
		return dir.y > 0 and "U" or "D"
	else
		return ( { [1] = "N", [0] = "", [-1] = "S" } )[ dz ] .. ( { [1] = "E", [0] = "", [-1] = "W" } )[ dx ]
	end
end

-----------------

globaltimer.start( 1.5, "update_hud", function ( )
	for name, data in pairs( registry.connected_players ) do
		local cur_pos = data.obj:get_pos( )
		local cur_dir = to_facing( data.obj:get_look_dir( ) )

		data.obj:hud_change( player_huds[ name ], "text",
			string.format( "You are facing %s at %s", dir_names[ cur_dir ], coords_to_string( cur_pos ) ) )
	end
end )

minetest.register_globalstep( function ( dtime )
	globaltimer.on_step( dtime )
end )

minetest.register_on_joinplayer( function( player )
	local player_name = player:get_player_name( )

	player_huds[ player_name ] = player:hud_add( {
		hud_elem_type = "",
		text = "Navigator Mod v1.0",
		position = { x = 0.5, y = 1 },
		scale = { x = -100, y = -100 },
		number = 0xFFFFFF,
		alignment = { x = 0, y = 0 },
		offset = { x = 0, y = -95 }
	} )
end )

minetest.register_on_leaveplayer( function( player )
	local player_name = player:get_player_name( )

	player_huds[ player_name ] = nil
end )
